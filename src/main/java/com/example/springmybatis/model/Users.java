package com.example.springmybatis.model;

public class Users {
    private int id;
    private String name;
    private String decription;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }
}
