package com.example.springmybatis.resource;

import com.example.springmybatis.mapper.UserMapper;
import com.example.springmybatis.model.Users;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private UserMapper userMapper;

    public UserController(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @PostMapping("/user")
    public ResponseEntity<String> saveUser(@RequestBody Users users){
        userMapper.save(users);
        return new ResponseEntity<>("SAVED", HttpStatus.CREATED);
    }

    @GetMapping("/user")
    public ResponseEntity<List<Users>> getAllUsers(){
        List<Users> users = userMapper.getAll();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Users> getUserById(@PathVariable("userId") int userId){
       return new ResponseEntity<>(userMapper.findById(userId),HttpStatus.OK);
    }

    @PutMapping("/user/{userId}")
    public ResponseEntity<String> updateUser(@RequestBody Users users,@PathVariable("userId") int userId){
        users.setId(userId);
        userMapper.updateUser(users);
        return new ResponseEntity<>("Updated",HttpStatus.OK);
    }

    @DeleteMapping("/user/{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable("userId") int userId){

        userMapper.deleteUser(userId);
        return new ResponseEntity<>("deleted",HttpStatus.OK);
    }

}
