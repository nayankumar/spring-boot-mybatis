package com.example.springmybatis.mapper;

import com.example.springmybatis.model.Users;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Insert("insert into users (name,decription) values (#{name},#{decription})")
    void save(Users users);

    @Select("Select * from users")
    List<Users> getAll();

    @Select("Select * from users where id =#{id}")
    Users findById(@Param("id") int userId);

    @Update("update users set name = #{name}, decription = #{decription} where id = #{id}")
    void updateUser(Users users);

    @Delete("delete from users where id = #{id}")
    void deleteUser(@Param("id") int userId);
}
